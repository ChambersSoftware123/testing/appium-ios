package tests;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.net.MalformedURLException;
import java.net.URL;

public class BaseTest {
    protected AppiumDriver<MobileElement> driver;

    @BeforeMethod
    public void setup() throws MalformedURLException {
        DesiredCapabilities desiredCapabilities = getDesiredCapabilities();
        driver = new AppiumDriver<>(new URL("http://localhost:4723/wd/hub"), desiredCapabilities);
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }

    private DesiredCapabilities getDesiredCapabilities() {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
        desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "16.2");
        desiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 14");
        desiredCapabilities.setCapability(MobileCapabilityType.APP, "/Users/thomaschambers/Library/Developer/Xcode/DerivedData/Bullseye-bbofmyszkoovhvezwuqgdyaxuktn/Build/Products/Debug-iphonesimulator/Bullseye.app");
        desiredCapabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
        desiredCapabilities.setCapability(MobileCapabilityType.NO_RESET, true);

        return desiredCapabilities;
    }
}
