package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.support.PageFactory;

public class BullsEyePage {
    private AppiumDriver<MobileElement> driver;

    @iOSXCUITFindBy(accessibility = "HIT ME")
    private MobileElement hitMeButton;

    public BullsEyePage(AppiumDriver<MobileElement> driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public void tapHitMeButton() {
        hitMeButton.click();
    }

}
